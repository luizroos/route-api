package br.com.luizroos.route.api.map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.luizroos.route.api.APIMediaTypes;
import br.com.luizroos.route.api.error.ErrorCodes;
import br.com.luizroos.route.api.exception.BadRequestException;
import br.com.luizroos.route.serv.city.UnknownCityException;
import br.com.luizroos.route.serv.map.MapService;
import br.com.luizroos.route.serv.map.ShippingPlan;
import br.com.luizroos.route.serv.path.UnknownRouteException;

/**
 * API de mapas
 * */
@RestController
public class MapResource {

	private static Logger LOGGER = LoggerFactory.getLogger(MapResource.class);

	@Autowired
	private MapService mapService;

	/**
	 * API de inclusão de mapa
	 * */
	@RequestMapping(value = "/maps", //
	method = RequestMethod.POST, //
	consumes = MediaType.APPLICATION_JSON_VALUE, //
	produces = APIMediaTypes.V1_JSON)
	public ResponseEntity<String> createJsonMap(@Valid @RequestBody CreateMapRequest createMapRequest) {
		LOGGER.debug("m=createMap");
		mapService.createMap(createMapRequest.getName(), createMapRequest.getPaths());
		return new ResponseEntity<String>(HttpStatus.NO_CONTENT);
	}

	@RequestMapping(value = "/maps/{mapName}/shipping-plans", //
	method = RequestMethod.GET, //
	produces = APIMediaTypes.V1_JSON)
	public ResponseEntity<FindShippingPlanResponse> findShippingPlan(@PathVariable("mapName") String mapName,
			@Valid FindShippingPlanRequest form) //
			throws BadRequestException {
		LOGGER.debug("m=findShippingPlan");
		try {
			final ShippingPlan shipping = mapService.findShippingPlan(mapName, form.getFrom(), form.getTo(),
					form.getBDAutonomy(), form.getBDGasPrice());
			final FindShippingPlanResponse findPathsResponse = new FindShippingPlanResponse(shipping);
			return new ResponseEntity<FindShippingPlanResponse>(findPathsResponse, HttpStatus.OK);
		} catch (UnknownCityException e) {
			throw new BadRequestException(ErrorCodes.E_FIND_SHIPPING_PLAN_UNKNOWN_CITY_EXC, e.getMap(), e.getName());
		} catch (UnknownRouteException e) {
			throw new BadRequestException(ErrorCodes.E_FIND_SHIPPING_PLAN_UNKNOWN_ROUTE_EXC, e.getMap(), e.getFrom(),
					e.getTo());
		}
	}

}
