package br.com.luizroos.route.api.map;

import java.math.BigDecimal;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;

import org.hibernate.validator.constraints.NotEmpty;

import br.com.luizroos.route.api.error.ErrorCodes;

/**
 * Formulário de busca de path
 * */
public class FindShippingPlanRequest {

	@NotEmpty(message = ErrorCodes.E_FIND_PATHS_EMPTY_FROM)
	private String from;

	@NotEmpty(message = ErrorCodes.E_FIND_PATHS_EMPTY_TO)
	private String to;

	@NotEmpty(message = ErrorCodes.E_FIND_PATHS_EMPTY_AUTONOMY)
	@Digits(integer = 5, fraction = 2, message = ErrorCodes.E_FIND_SHIPPING_PLAN_INVALID_AUTONOMY)
	@Min(value = 0, message = ErrorCodes.E_FIND_SHIPPING_PLAN_INVALID_AUTONOMY)
	private String autonomy;

	@NotEmpty(message = ErrorCodes.E_FIND_PATHS_EMPTY_GAS_PRICE)
	@Digits(integer = 2, fraction = 2, message = ErrorCodes.E_FIND_SHIPPING_PLAN_INVALID_GAS_PRICE)
	@Min(value = 0, message = ErrorCodes.E_FIND_SHIPPING_PLAN_INVALID_GAS_PRICE)
	private String gasPrice;

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getAutonomy() {
		return autonomy;
	}

	public void setAutonomy(String autonomy) {
		this.autonomy = autonomy;
	}

	public String getGasPrice() {
		return gasPrice;
	}

	public void setGasPrice(String gasPrice) {
		this.gasPrice = gasPrice;
	}

	public BigDecimal getBDGasPrice() {
		return new BigDecimal(getGasPrice());
	}

	public BigDecimal getBDAutonomy() {
		return new BigDecimal(getAutonomy());
	}
}
