package br.com.luizroos.route.api.map;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;

import org.hibernate.validator.constraints.NotEmpty;

import br.com.luizroos.route.api.error.ErrorCodes;
import br.com.luizroos.route.serv.path.Path;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Objeto de request de criação de mapa
 * */
public class CreatePathRequest implements Path {

	@JsonProperty(value = "to")
	@NotEmpty(message = ErrorCodes.E_CREATE_MAP_EMPTY_TO)
	private String to;

	@JsonProperty(value = "from")
	@NotEmpty(message = ErrorCodes.E_CREATE_MAP_EMPTY_FROM)
	private String from;

	@JsonProperty(value = "distance")
	@NotEmpty(message = ErrorCodes.E_CREATE_MAP_EMPTY_DISTANCE)
	@Digits(integer = 5, fraction = 0, message = ErrorCodes.E_CREATE_MAP_INVALID_DISTANCE)
	@Min(value = 1, message = ErrorCodes.E_CREATE_MAP_INVALID_DISTANCE)
	private String stringDistance;

	@Override
	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getStringDistance() {
		return stringDistance;
	}

	public void setStringDistance(String stringDistance) {
		this.stringDistance = stringDistance;
	}

	@Override
	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	@JsonIgnore
	@Override
	public int getDistance() {
		return Integer.valueOf(stringDistance);
	}

}
