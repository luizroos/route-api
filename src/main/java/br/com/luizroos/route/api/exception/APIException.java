package br.com.luizroos.route.api.exception;

import org.springframework.http.HttpStatus;

/**
 * Exceção da camada de APIs, serve para criar exceções da camada de api da
 * aplicação.
 * */
public abstract class APIException extends Exception {

	private static final long serialVersionUID = 1L;

	private final HttpStatus status;

	public APIException(HttpStatus status) {
		if (status == null) {
			throw new IllegalArgumentException();
		}
		this.status = status;
	}

	public HttpStatus getStatus() {
		return status;
	}
}
