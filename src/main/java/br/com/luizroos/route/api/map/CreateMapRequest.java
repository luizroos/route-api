package br.com.luizroos.route.api.map;

import java.util.List;

import javax.validation.Valid;

import org.hibernate.validator.constraints.NotEmpty;

import br.com.luizroos.route.api.error.ErrorCodes;

/**
 * Objeto de request de criação de mapa
 * */
public class CreateMapRequest {

	@NotEmpty(message = ErrorCodes.E_CREATE_MAP_EMPTY_NAME)
	private String name;

	@Valid
	@NotEmpty(message = ErrorCodes.E_CREATE_MAP_EMPTY_PATHS)
	private List<CreatePathRequest> paths;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<CreatePathRequest> getPaths() {
		return paths;
	}

	public void setPaths(List<CreatePathRequest> paths) {
		this.paths = paths;
	}

}
