package br.com.luizroos.route.api.error;

/**
 * Definição de um erro de API
 * */
public class ErrorInfo implements Comparable<ErrorInfo> {

	public final String code;

	public final String message;

	public ErrorInfo(String code, String message) {
		if (code == null || message == null) {
			throw new IllegalArgumentException();
		}
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

	@Override
	public int compareTo(ErrorInfo o) {
		return code.compareTo(o.code);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ErrorInfo other = (ErrorInfo) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		return true;
	}

}
