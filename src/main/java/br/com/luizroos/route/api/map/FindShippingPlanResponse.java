package br.com.luizroos.route.api.map;

import java.math.BigDecimal;

import br.com.luizroos.route.serv.map.ShippingPlan;
import br.com.luizroos.route.serv.path.Path;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.common.base.Function;
import com.google.common.collect.Lists;

/**
 * Resposta da operação de busca de paths
 * */
@JsonPropertyOrder({ "distance", "shippingCost", "path" })
public class FindShippingPlanResponse {

	private static Function<Path, PathResponse> PATH_TO_PATH_RESPONSE_FUNCTION = new Function<Path, FindShippingPlanResponse.PathResponse>() {
		@Override
		public PathResponse apply(Path path) {
			return new PathResponse(path);
		}
	};

	private final ShippingPlan shippingPlan;

	public FindShippingPlanResponse(ShippingPlan shippingPlan) {
		if (shippingPlan == null) {
			throw new IllegalArgumentException();
		}
		this.shippingPlan = shippingPlan;
	}

	public Iterable<PathResponse> getPaths() {
		return Lists.transform(shippingPlan.getRoute().getPaths(), PATH_TO_PATH_RESPONSE_FUNCTION);
	}

	public BigDecimal getShippingCost() {
		return shippingPlan.getShippingCost();
	}

	public double getDistance() {
		return shippingPlan.getRoute().getDistance();
	}

	// inner class
	@JsonPropertyOrder({ "from", "to", "distance" })
	static class PathResponse {

		private final Path path;

		private PathResponse(Path path) {
			if (path == null) {
				throw new IllegalArgumentException();
			}
			this.path = path;
		}

		public String getFrom() {
			return path.getFrom();
		}

		public String getTo() {
			return path.getTo();
		}

		public int getDistance() {
			return path.getDistance();
		}

	}
}
