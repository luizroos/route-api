package br.com.luizroos.route.api.exception;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import br.com.luizroos.route.api.error.ErrorInfo;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

/**
 * Handler de exceções da API
 * */
@ControllerAdvice
public class GlobalExceptionHandler {

	private static Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionHandler.class);

	@Autowired
	private MessageSource messageSource;

	@ExceptionHandler(Exception.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	protected void processException(Exception ex) {
		ex.printStackTrace();
		logE(ex);
	}

	@ExceptionHandler(HttpRequestMethodNotSupportedException.class)
	@ResponseStatus(value = HttpStatus.METHOD_NOT_ALLOWED)
	protected void processHttpRequestMethodNotSupportedException(HttpRequestMethodNotSupportedException ex) {
		logE(ex);
	}

	@ExceptionHandler(HttpMessageNotReadableException.class)
	@ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY)
	protected void processHttpMessageNotReadableException(HttpMessageNotReadableException ex) throws IOException {
		logE(ex);
	}

	@ExceptionHandler(HttpMediaTypeNotAcceptableException.class)
	@ResponseStatus(value = HttpStatus.NOT_ACCEPTABLE)
	protected void processHttpMediaTypeNotAcceptableException(HttpMediaTypeNotAcceptableException ex) {
		logE(ex);
	}

	@ExceptionHandler(HttpMediaTypeNotSupportedException.class)
	@ResponseStatus(value = HttpStatus.UNSUPPORTED_MEDIA_TYPE)
	protected void processHttpMediaTypeNotSupportedException(HttpMediaTypeNotSupportedException ex) {
		logE(ex);
	}

	@ExceptionHandler(BindException.class)
	public ResponseEntity<List<ErrorInfo>> processBindException(BindException ex) {
		logE(ex);
		return badRequest(ex.getAllErrors());
	}

	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<List<ErrorInfo>> processMethodArgumentNotValidException(MethodArgumentNotValidException ex) {
		logE(ex);
		return badRequest(ex.getBindingResult().getFieldErrors());
	}

	@ExceptionHandler(BadRequestException.class)
	public ResponseEntity<List<ErrorInfo>> processBadRequestException(BadRequestException ex) {
		logE(ex);
		final String code = ex.getErrorCode();
		final ErrorInfo error = new ErrorInfo(code, getMessage(code, ex.getArgs()));
		return errorInfoBody(Sets.newHashSet(error), ex.getStatus());
	}

	private String getMessage(String code, Object... args) {
		return messageSource.getMessage(code, args, Locale.getDefault());
	}

	private ResponseEntity<List<ErrorInfo>> badRequest(List<? extends ObjectError> oErrors) {
		if (oErrors == null) {
			throw new IllegalArgumentException();
		}
		final Set<ErrorInfo> errors = Sets.newHashSet();
		for (ObjectError oError : oErrors) {
			final String code = oError.getDefaultMessage();
			final String message = getMessage(code, oError.getArguments());
			final ErrorInfo error = new ErrorInfo(code, message);
			errors.add(error);
		}
		return errorInfoBody(errors, HttpStatus.BAD_REQUEST);
	}

	private ResponseEntity<List<ErrorInfo>> errorInfoBody(Set<ErrorInfo> errors, HttpStatus httpStatus) {
		if (errors == null || httpStatus == null) {
			throw new IllegalArgumentException();
		}
		final ArrayList<ErrorInfo> listErrors = Lists.newArrayList(errors);
		Collections.sort(listErrors);
		return new ResponseEntity<List<ErrorInfo>>(listErrors, httpStatus);
	}

	private static void logE(Exception e) {
		LOGGER.debug("e={},m={}", e.getClass().getSimpleName(), e.getMessage());
	}

}
