package br.com.luizroos.route.serv.city;

/**
 * Deinição dos serviços de cidade
 * */
public interface CityService {

	/**
	 * Inclui uma nova cidade
	 */
	CityEntity createCity(String map, String cityName);

	/**
	 * Busca uma cidade, se não exitir, cria ela
	 */
	CityEntity findOrCreate(String map, String name);

}
