package br.com.luizroos.route.serv.path;

import java.util.List;

/**
 * Implementacao default de uma {@link Route}
 * */
public class DefaultRoute implements Route {

	private final double distance;

	private final List<? extends Path> paths;

	public DefaultRoute(List<? extends Path> paths) {
		if (paths == null || paths.size() == 0) {
			throw new IllegalArgumentException();
		}
		int dist = 0;
		for (Path path : paths) {
			dist += path.getDistance();
		}
		this.distance = dist;
		this.paths = paths;
	}

	@Override
	public double getDistance() {
		return distance;
	}

	@Override
	public List<? extends Path> getPaths() {
		return paths;
	}
}
