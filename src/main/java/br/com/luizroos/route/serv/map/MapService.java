package br.com.luizroos.route.serv.map;

import java.math.BigDecimal;

import br.com.luizroos.route.serv.city.UnknownCityException;
import br.com.luizroos.route.serv.path.Path;
import br.com.luizroos.route.serv.path.UnknownRouteException;

/**
 * Deinição dos serviços de mapas
 * */
public interface MapService {

	/**
	 * Cria um mapa
	 * */
	int createMap(String name, Iterable<? extends Path> paths);

	/**
	 * Calcula o plano de entrega entre duas cidades para um mapa
	 * */
	ShippingPlan findShippingPlan(String map, String from, String to, BigDecimal autonomy, BigDecimal gasPrice)
			throws UnknownCityException, UnknownRouteException;

}
