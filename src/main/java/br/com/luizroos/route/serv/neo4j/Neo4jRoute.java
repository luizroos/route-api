package br.com.luizroos.route.serv.neo4j;

import java.util.Iterator;
import java.util.List;

import org.neo4j.graphalgo.WeightedPath;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;

import br.com.luizroos.route.serv.city.CityEntity;
import br.com.luizroos.route.serv.path.DefaultPath;
import br.com.luizroos.route.serv.path.Path;
import br.com.luizroos.route.serv.path.PathEntity;
import br.com.luizroos.route.serv.path.Route;

import com.google.common.collect.Lists;

/**
 * Implementacao de rota com neo4j
 * */
public class Neo4jRoute implements Route {

	private final double distance;

	private final List<Path> paths;

	public Neo4jRoute(WeightedPath weightedPath) {
		if (weightedPath == null) {
			throw new IllegalArgumentException();
		}

		// cria a rota
		this.paths = Lists.newArrayList();
		String lastCity = null;
		final Iterator<Relationship> relationships = weightedPath.relationships().iterator();
		for (Node node : weightedPath.nodes()) {
			final String cityName = (String) node.getProperty(CityEntity.NAME_PROPERTY);
			if (lastCity == null) {
				lastCity = cityName;
			} else {
				final Integer distance = (Integer) relationships.next().getProperty(PathEntity.DISTANCE_PROPERTY_NAME);
				final Path path = new DefaultPath(lastCity, cityName, distance);
				paths.add(path);
				lastCity = cityName;
			}
		}

		this.distance = weightedPath.weight();
	}

	@Override
	public double getDistance() {
		return distance;
	}

	@Override
	public List<? extends Path> getPaths() {
		return paths;
	}

}
