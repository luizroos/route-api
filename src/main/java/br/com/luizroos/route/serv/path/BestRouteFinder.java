package br.com.luizroos.route.serv.path;

import br.com.luizroos.route.serv.city.CityEntity;

/**
 * Interface que define a busca de melhor rota entre duas cidades
 * */
public interface BestRouteFinder {

	/**
	 * Deve retornar a melhor rota entre as duas cidades
	 * */
	Route find(String map, CityEntity from, CityEntity to) throws UnknownRouteException;

}
