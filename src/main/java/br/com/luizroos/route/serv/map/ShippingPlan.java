package br.com.luizroos.route.serv.map;

import java.math.BigDecimal;

import br.com.luizroos.route.serv.path.Route;

/**
 * Plano de entrega, envolve um custo e uma rota
 * */
public class ShippingPlan {

	private final Route route;

	private final BigDecimal shippingCost;

	public ShippingPlan(Route route, BigDecimal autonomy, BigDecimal gasPrice) {
		if (route == null || autonomy == null || gasPrice == null || autonomy.compareTo(BigDecimal.ZERO) < 0
				|| gasPrice.compareTo(BigDecimal.ZERO) < 0) {
			throw new IllegalArgumentException();
		}
		this.shippingCost = gasPrice.multiply(new BigDecimal(route.getDistance())).divide(autonomy).setScale(2);
		this.route = route;
	}

	public Route getRoute() {
		return route;
	}

	public BigDecimal getShippingCost() {
		return shippingCost;
	}

}
