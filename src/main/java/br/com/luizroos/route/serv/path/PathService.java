package br.com.luizroos.route.serv.path;

import br.com.luizroos.route.serv.city.UnknownCityException;

/**
 * Definição dos serviços de caminho
 * */
public interface PathService {

	/**
	 * Busca o melhor caminho entre duas cidades
	 */
	Route findBestRoute(String map, String from, String to) throws UnknownCityException, UnknownRouteException;

	/**
	 * Cria um path entre duas cidades. Se a cidade não existir, ela é criada
	 */
	PersistedPath createPathBetween(String map, String fromCityName, String toCityName, int distance);

}
