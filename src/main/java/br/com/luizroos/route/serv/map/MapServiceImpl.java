package br.com.luizroos.route.serv.map;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.luizroos.route.serv.city.UnknownCityException;
import br.com.luizroos.route.serv.path.Path;
import br.com.luizroos.route.serv.path.PathService;
import br.com.luizroos.route.serv.path.Route;
import br.com.luizroos.route.serv.path.UnknownRouteException;

@Service
public class MapServiceImpl implements MapService {

	private static Logger LOGGER = LoggerFactory.getLogger(MapServiceImpl.class);

	@Autowired
	private PathService pathService;

	@Override
	public int createMap(String name, Iterable<? extends Path> paths) {
		if (name == null || paths == null) {
			throw new IllegalArgumentException();
		}
		LOGGER.debug("m=createMap,name={}", name);
		for (Path path : paths) {
			pathService.createPathBetween(name, path.getFrom(), path.getTo(), path.getDistance());
		}
		return 1;
	}

	@Override
	public ShippingPlan findShippingPlan(String map, String from, String to, BigDecimal autonomy, BigDecimal gasPrice)
			throws UnknownCityException, UnknownRouteException {
		if (map == null || from == null || to == null || autonomy == null || gasPrice == null
				|| autonomy.compareTo(BigDecimal.ZERO) < 0 || gasPrice.compareTo(BigDecimal.ZERO) < 0) {
			throw new IllegalArgumentException();
		}

		final Route route = pathService.findBestRoute(map, from, to);
		return new ShippingPlan(route, autonomy, gasPrice);
	}

}
