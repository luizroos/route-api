package br.com.luizroos.route.serv.path;

/**
 * exceção lançada quando não existe rota entre duas cidades
 * */
public class UnknownRouteException extends Exception {

	private static final long serialVersionUID = 1L;

	private final String map;

	private final String from;

	private final String to;

	public UnknownRouteException(String map, String from, String to) {
		if (map == null || from == null || to == null) {
			throw new IllegalArgumentException();
		}
		this.map = map;
		this.from = from;
		this.to = to;
	}

	public String getMap() {
		return map;
	}

	public String getFrom() {
		return from;
	}

	public String getTo() {
		return to;
	}

}
