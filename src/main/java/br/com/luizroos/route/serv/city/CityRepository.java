package br.com.luizroos.route.serv.city;

import org.springframework.data.repository.CrudRepository;

public interface CityRepository extends CrudRepository<CityEntity, Long> {

	CityEntity findByUniqueId(String uniqueId);

}