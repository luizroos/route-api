package br.com.luizroos.route.serv.path;

import org.neo4j.graphdb.RelationshipType;
import org.springframework.data.neo4j.annotation.EndNode;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.GraphProperty;
import org.springframework.data.neo4j.annotation.RelationshipEntity;
import org.springframework.data.neo4j.annotation.StartNode;

import br.com.luizroos.route.serv.city.CityEntity;
import br.com.luizroos.route.serv.path.PathEntity.PathType;

@RelationshipEntity(type = PathType.PATH_VALUE)
public class PathEntity implements PersistedPath {

	public final static String DISTANCE_PROPERTY_NAME = "distance";

	public enum PathType implements RelationshipType {

		PATH;

		final static String PATH_VALUE = "PATH";
	}

	@GraphId
	private Long id;

	@StartNode
	private CityEntity fromCity;

	@EndNode
	private CityEntity toCity;

	@GraphProperty(propertyName = DISTANCE_PROPERTY_NAME)
	private int distance;

	public PathEntity() {
	}

	public PathEntity(CityEntity fromCity, CityEntity toCity, int distance) {
		if (fromCity == null || toCity == null || distance <= 0) {
			throw new IllegalArgumentException();
		}
		this.fromCity = fromCity;
		this.toCity = toCity;
		this.distance = distance;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public CityEntity getFromCity() {
		return fromCity;
	}

	public void setFromCity(CityEntity fromCity) {
		this.fromCity = fromCity;
	}

	public CityEntity getToCity() {
		return toCity;
	}

	public void setToCity(CityEntity toCity) {
		this.toCity = toCity;
	}

	public int getDistance() {
		return distance;
	}

	public void setDistance(int distance) {
		this.distance = distance;
	}

	@Override
	public String getFrom() {
		return getFromCity().getName();
	}

	@Override
	public String getTo() {
		return getToCity().getName();
	}

}
