package br.com.luizroos.route.serv.path;

/**
 * Implementacao default de um {@link Path}
 * */
public class DefaultPath implements Path {

	private final String from;

	private final String to;

	private final int distance;

	public DefaultPath(String from, String to, int distance) {
		if (from == null || to == null || distance <= 0) {
			throw new IllegalArgumentException();
		}
		this.from = from;
		this.to = to;
		this.distance = distance;
	}

	public String getFrom() {
		return from;
	}

	public String getTo() {
		return to;
	}

	public int getDistance() {
		return distance;
	}

}
