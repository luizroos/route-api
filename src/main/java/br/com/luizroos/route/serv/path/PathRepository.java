package br.com.luizroos.route.serv.path;

import org.springframework.data.repository.CrudRepository;

public interface PathRepository extends CrudRepository<PathEntity, Long> {

}
