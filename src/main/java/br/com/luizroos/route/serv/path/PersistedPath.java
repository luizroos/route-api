package br.com.luizroos.route.serv.path;

/**
 * Path persistido, basicamente tem um id
 * */
public interface PersistedPath extends Path {

	/**
	 * Id do path
	 * */
	Long getId();

}
