package br.com.luizroos.route.serv.neo4j;

import org.neo4j.graphalgo.GraphAlgoFactory;
import org.neo4j.graphalgo.PathFinder;
import org.neo4j.graphalgo.WeightedPath;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.PathExpanders;
import org.neo4j.graphdb.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.neo4j.support.Neo4jTemplate;
import org.springframework.stereotype.Service;

import br.com.luizroos.route.serv.city.CityEntity;
import br.com.luizroos.route.serv.path.BestRouteFinder;
import br.com.luizroos.route.serv.path.PathEntity;
import br.com.luizroos.route.serv.path.PathEntity.PathType;
import br.com.luizroos.route.serv.path.PathServiceImpl;
import br.com.luizroos.route.serv.path.Route;
import br.com.luizroos.route.serv.path.UnknownRouteException;

/**
 * Busca a melhor rota usando neo4j
 * */
@Service
public class Neo4jBestRouteFinder implements BestRouteFinder {

	private static Logger LOGGER = LoggerFactory.getLogger(PathServiceImpl.class);

	@Autowired
	private Neo4jTemplate template;

	@Override
	public Route find(String map, CityEntity from, CityEntity to) throws UnknownRouteException {
		try (Transaction tx = template.getGraphDatabaseService().beginTx()) {
			LOGGER.debug("m=findBestRoute,ac=executando_dijkstra");
			final PathFinder<WeightedPath> finder = GraphAlgoFactory.dijkstra(
					PathExpanders.forTypeAndDirection(PathType.PATH, Direction.BOTH), //
					PathEntity.DISTANCE_PROPERTY_NAME);

			LOGGER.debug("m=findBestRoute,ac=executando_findSinglePath");
			final WeightedPath weightefPath = finder.findSinglePath(//
					template.getNode(from.getId()), //
					template.getNode(to.getId()));

			LOGGER.debug("m=findBestRoute,path={}", weightefPath != null);
			if (weightefPath == null) {
				throw new UnknownRouteException(map, from.getName(), to.getName());
			}
			return new Neo4jRoute(weightefPath);
		}
	}

}
