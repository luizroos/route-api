package br.com.luizroos.route.serv.city;

/**
 * Exceção para indicar que a cidade não existe
 * */
public class UnknownCityException extends Exception {

	private static final long serialVersionUID = 1L;

	private final String name;

	private final String map;

	public UnknownCityException(String map, String name) {
		if (map == null || name == null) {
			throw new IllegalArgumentException();
		}
		this.name = name;
		this.map = map;
	}

	public String getMap() {
		return map;
	}

	public String getName() {
		return name;
	}

}
