package br.com.luizroos.route.serv.city;

import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.GraphProperty;
import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.support.index.IndexType;

@NodeEntity
public class CityEntity {

	public final static String NAME_PROPERTY = "name";

	@GraphId
	private Long id;

	@Indexed
	@GraphProperty(propertyName = NAME_PROPERTY)
	private String name;

	@GraphProperty(propertyName = "map")
	private String map;

	@Indexed(unique = true, failOnDuplicate = true, indexType = IndexType.LABEL)
	private String uniqueId;

	public CityEntity() {
	}

	public CityEntity(String map, String name) {
		if (map == null || name == null) {
			throw new IllegalArgumentException();
		}
		this.map = map.toLowerCase();
		this.name = name;
		this.uniqueId = uniqueId(map, name);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUniqueId() {
		return uniqueId;
	}

	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}

	public String getMap() {
		return map;
	}

	public void setMap(String map) {
		this.map = map;
	}

	// static methods

	public static String uniqueId(String map, String name) {
		if (map == null || name == null) {
			throw new IllegalArgumentException();
		}
		return map.toLowerCase() + "-" + name;
	}

}