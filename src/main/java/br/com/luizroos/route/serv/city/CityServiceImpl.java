package br.com.luizroos.route.serv.city;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Implementacao default {@link CityService}
 * */
@Service
public class CityServiceImpl implements CityService {

	private static Logger LOGGER = LoggerFactory.getLogger(CityServiceImpl.class);

	@Autowired
	private CityRepository cityRepository;

	@Override
	public CityEntity createCity(String map, String name) {
		if (map == null || name == null) {
			throw new IllegalArgumentException();
		}
		LOGGER.debug("m=create,map={},name={}", map, name);
		final CityEntity entity = new CityEntity(map, name);
		cityRepository.save(entity);
		return entity;
	}

	@Override
	public CityEntity findOrCreate(String map, String name) {
		if (map == null || name == null) {
			throw new IllegalArgumentException();
		}
		LOGGER.debug("m=findOrCreate,map={},name={}", map, name);
		final CityEntity cityEntity = cityRepository.findByUniqueId(CityEntity.uniqueId(map, name));
		if (cityEntity != null) {
			return cityEntity;
		}
		return createCity(map, name);
	}

}
