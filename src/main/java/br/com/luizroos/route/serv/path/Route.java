package br.com.luizroos.route.serv.path;

import java.util.List;

/**
 * Representa uma rota: a distancia total e uma sequencia de paths ordenados
 * */
public interface Route {

	double getDistance();

	List<? extends Path> getPaths();

}
