package br.com.luizroos.route.serv.neo4j;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.neo4j.config.EnableNeo4jRepositories;
import org.springframework.data.neo4j.config.Neo4jConfiguration;

@Configuration
@EnableNeo4jRepositories(basePackages = Neo4jConstants.BASE_PACKAGE)
@Profile("prod")
public class Neo4jConfig extends Neo4jConfiguration {

	public Neo4jConfig() {
		setBasePackage(Neo4jConstants.BASE_PACKAGE);
	}

	@Bean(destroyMethod = "shutdown")
	public GraphDatabaseService graphDatabaseService(@Value("${neo4j.db.location}") String neo4jDatabaseLocation) {
		return new GraphDatabaseFactory().newEmbeddedDatabase(neo4jDatabaseLocation);
	}
}
