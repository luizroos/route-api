package br.com.luizroos.route.serv.path;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.luizroos.route.serv.city.CityEntity;
import br.com.luizroos.route.serv.city.CityRepository;
import br.com.luizroos.route.serv.city.CityService;
import br.com.luizroos.route.serv.city.UnknownCityException;

/**
 * Implementação default de {@link PathService}
 * */
@Service
public class PathServiceImpl implements PathService {

	private static Logger LOGGER = LoggerFactory.getLogger(PathServiceImpl.class);

	@Autowired
	private CityService cityService;

	@Autowired
	private CityRepository cityRepository;

	@Autowired
	private PathRepository pathRepository;

	@Autowired
	private BestRouteFinder bestRouteFinder;

	@Override
	public Route findBestRoute(String map, String from, String to) throws UnknownCityException, UnknownRouteException {
		if (map == null || from == null || to == null) {
			throw new IllegalArgumentException();
		}

		LOGGER.debug("m=findBestRoute,map={},from={},to={}", map, from, to);

		// busca a cidade de origem
		final CityEntity fromCity = cityRepository.findByUniqueId(CityEntity.uniqueId(map, from));
		if (fromCity == null) {
			throw new UnknownCityException(map, from);
		}

		// busca a cidade de destino
		final CityEntity toCity = cityRepository.findByUniqueId(CityEntity.uniqueId(map, to));
		if (toCity == null) {
			throw new UnknownCityException(map, to);
		}

		return bestRouteFinder.find(map, fromCity, toCity);
	}

	@Override
	public PersistedPath createPathBetween(String map, String from, String to, int distance) {
		if (map == null || from == null || to == null || distance <= 0) {
			throw new IllegalArgumentException();
		}
		LOGGER.debug("m=createPathBetween,from={},to={},distance={}", from, to, distance);

		// busca ou cria a cidade de origem
		final CityEntity fromCity = cityService.findOrCreate(map, from);

		// busca ou cria a cidade de destino
		final CityEntity toCity = cityService.findOrCreate(map, to);

		return createPathBetween(fromCity, toCity, distance);
	}

	private PersistedPath createPathBetween(CityEntity fromCity, CityEntity toCity, int distance) {
		if (fromCity == null || toCity == null || distance < 0) {
			throw new IllegalArgumentException();
		}

		// não é necessário criar dois paths
		// (https://dzone.com/articles/modelling-data-neo4j), mas deve-se
		// lembrar sempre de ignorar a direção do relacionamento

		// cria e salva o path entre as duas cidades
		final PathEntity path = new PathEntity(fromCity, toCity, distance);
		pathRepository.save(path);

		return path;
	}

}
