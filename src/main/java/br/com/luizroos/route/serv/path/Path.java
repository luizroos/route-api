package br.com.luizroos.route.serv.path;

/**
 * Definição de um path
 * */
public interface Path {

	/**
	 * Deve retornar a origem do path
	 * */
	String getFrom();

	/**
	 * Deve retornar o destino do path
	 * */
	String getTo();

	/**
	 * Deve retornar a distancia entre origem e destino
	 * */
	int getDistance();
}
