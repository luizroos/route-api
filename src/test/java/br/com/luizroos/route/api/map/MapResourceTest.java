package br.com.luizroos.route.api.map;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.util.ArrayList;

import junit.framework.TestCase;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import br.com.luizroos.route.Application;
import br.com.luizroos.route.TestConstants;
import br.com.luizroos.route.api.APIMediaTypes;
import br.com.luizroos.route.serv.map.MapService;
import br.com.luizroos.route.serv.map.ShippingPlan;
import br.com.luizroos.route.serv.path.DefaultPath;
import br.com.luizroos.route.serv.path.DefaultRoute;
import br.com.luizroos.route.serv.path.Route;

import com.google.common.collect.Lists;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@ActiveProfiles(value = TestConstants.TEST_PROFILE)
@WebAppConfiguration
@TestPropertySource(locations = "classpath:/map-resource-test.properties")
public class MapResourceTest {

	@Autowired
	private Environment environment;

	private MockMvc mvc;

	@Mock
	private MapService mapService;

	@InjectMocks
	private MapResource mapResource;

	@SuppressWarnings("unchecked")
	@Test
	public void testPostMap() throws Exception {
		initMocks(this);
		when(mapService.createMap(Mockito.anyString(), Mockito.anyList())).thenReturn(1);
		this.mvc = MockMvcBuilders.standaloneSetup(mapResource).build();
		mvc.perform(post("/maps") //
				.accept(APIMediaTypes.V1_JSON) //
				.contentType(MediaType.APPLICATION_JSON) //
				.content(environment.getProperty("testPostMap.request"))) //
				.andExpect(status().isNoContent());
	}

	
	@Test
	public void testGetShippingPlan() throws Exception {
		initMocks(this);
		final ArrayList<DefaultPath> paths = Lists.newArrayList(//
				new DefaultPath("A", "B", 10), //
				new DefaultPath("B", "C", 15));
		final Route route = new DefaultRoute(paths);
		final ShippingPlan shippingPlan = new ShippingPlan(route, BigDecimal.TEN, new BigDecimal(2.5));
		when(mapService.findShippingPlan("teste", "A", "C", BigDecimal.TEN, new BigDecimal(2.5))).thenReturn(
				shippingPlan);

		this.mvc = MockMvcBuilders.standaloneSetup(mapResource).build();
		final String content = mvc.perform(get("/maps/teste/shipping-plans?from=A&to=C&autonomy=10&gasPrice=2.5") //
				.accept(APIMediaTypes.V1_JSON)) //
				.andExpect(status().isOk()) //
				.andReturn().getResponse().getContentAsString();
		System.out.println(content);
		TestCase.assertEquals(environment.getProperty("testGetShippingPlan.response"), content);
	}
}
