package br.com.luizroos.route.serv.neo4j;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.test.TestGraphDatabaseFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.neo4j.config.EnableNeo4jRepositories;
import org.springframework.data.neo4j.config.Neo4jConfiguration;

import br.com.luizroos.route.TestConstants;
import br.com.luizroos.route.serv.neo4j.Neo4jConstants;

@Configuration
@EnableNeo4jRepositories(basePackages = Neo4jConstants.BASE_PACKAGE)
@Profile(TestConstants.TEST_PROFILE)
public class TestNeo4jConfig extends Neo4jConfiguration {

	public TestNeo4jConfig() {
		setBasePackage(Neo4jConstants.BASE_PACKAGE);
	}

	@Bean(destroyMethod = "shutdown")
	public GraphDatabaseService graphDatabaseService() {
		return new TestGraphDatabaseFactory().newImpermanentDatabase();
	}
}
