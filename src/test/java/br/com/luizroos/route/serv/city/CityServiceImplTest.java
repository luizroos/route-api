package br.com.luizroos.route.serv.city;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.com.luizroos.route.Application;
import br.com.luizroos.route.TestConstants;
import br.com.luizroos.route.TestDataFactory;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@ActiveProfiles(value = TestConstants.TEST_PROFILE)
public class CityServiceImplTest {

	@Autowired
	private CityServiceImpl cityService;

	@Autowired
	private CityRepository cityRepository;

	private String mapName;

	@Before
	public void setUp() {
		mapName = TestDataFactory.newMapName();
	}

	@Test
	public void testCreateCity() {
		final String cityName = "Joinville";
		final CityEntity e = cityService.createCity(mapName, cityName);
		TestCase.assertNotNull(e.getId());
		TestCase.assertEquals(e.getName(), cityName);
		TestCase.assertEquals(e.getMap(), mapName.toLowerCase());
		TestCase.assertEquals(e.getUniqueId(), CityEntity.uniqueId(mapName, cityName));
	}

	@Test(expected = DataIntegrityViolationException.class)
	public void testCreateAlreadyExistsCity() {
		final String cityName = "Sao Paulo";
		cityService.createCity(mapName, cityName);
		cityService.createCity(mapName, cityName);
	}

	@Test
	public void testFindOrCreate() {
		final String cityName = "Florianopolis";
		final String uniqueId = CityEntity.uniqueId(mapName, cityName);
		final CityEntity existsBefore = cityRepository.findByUniqueId(uniqueId);
		TestCase.assertNull(existsBefore);
		final CityEntity created = cityService.findOrCreate(mapName, cityName);
		TestCase.assertNotNull(created);
		final CityEntity existsAfter = cityRepository.findByUniqueId(uniqueId);
		TestCase.assertEquals(existsAfter.getId(), created.getId());
	}
}
