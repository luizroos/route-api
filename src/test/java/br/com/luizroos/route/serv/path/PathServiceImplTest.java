package br.com.luizroos.route.serv.path;

import java.util.List;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.com.luizroos.route.Application;
import br.com.luizroos.route.TestConstants;
import br.com.luizroos.route.TestDataFactory;
import br.com.luizroos.route.serv.city.UnknownCityException;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@ActiveProfiles(value = TestConstants.TEST_PROFILE)
public class PathServiceImplTest {

	@Autowired
	private PathServiceImpl pathService;

	private String mapName;

	@Before
	public void setUp() {
		mapName = TestDataFactory.newMapName();
	}

	@Test
	public void testCreatePath() {
		final int distance = 10;
		final String from = "Campinas";
		final String to = "Araraquara";
		final PersistedPath path = pathService.createPathBetween(mapName, from, to, distance);
		TestCase.assertNotNull(path.getId());
		TestCase.assertEquals(path.getDistance(), distance);
		TestCase.assertEquals(path.getFrom(), from);
		TestCase.assertEquals(path.getTo(), to);
	}

	@Test(expected = UnknownCityException.class)
	public void testFindBestRouteWithInexistentCity() throws UnknownCityException, UnknownRouteException {
		pathService.findBestRoute(mapName, "A", "D");
	}

	@Test(expected = UnknownRouteException.class)
	public void testyUnknownRoute() throws UnknownCityException, UnknownRouteException {
		pathService.createPathBetween(mapName, "A", "B", 10);
		pathService.createPathBetween(mapName, "C", "D", 5);
		pathService.findBestRoute(mapName, "A", "D");
	}

	@Test
	public void testFindBestRoute() throws UnknownCityException, UnknownRouteException {
		pathService.createPathBetween(mapName, "A", "B", 10);
		pathService.createPathBetween(mapName, "B", "C", 5);
		pathService.createPathBetween(mapName, "C", "D", 15);
		pathService.createPathBetween(mapName, "A", "C", 11);
		final Route route = pathService.findBestRoute(mapName, "A", "D");
		TestCase.assertEquals(route.getDistance(), 26D);
		final List<? extends Path> paths = route.getPaths();
		TestCase.assertEquals(paths.size(), 2);
		assertPath(paths.get(0), "A", "C", 11);
		assertPath(paths.get(1), "C", "D", 15);
	}

	private static void assertPath(Path path, String from, String to, int distance) {
		TestCase.assertEquals(path.getDistance(), distance);
		TestCase.assertEquals(path.getFrom(), from);
		TestCase.assertEquals(path.getTo(), to);
	}

}
