package br.com.luizroos.route.serv.map;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import java.math.BigDecimal;
import java.util.ArrayList;

import junit.framework.TestCase;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.com.luizroos.route.Application;
import br.com.luizroos.route.TestConstants;
import br.com.luizroos.route.TestDataFactory;
import br.com.luizroos.route.serv.city.UnknownCityException;
import br.com.luizroos.route.serv.path.PathService;
import br.com.luizroos.route.serv.path.Route;
import br.com.luizroos.route.serv.path.DefaultPath;
import br.com.luizroos.route.serv.path.DefaultRoute;
import br.com.luizroos.route.serv.path.UnknownRouteException;

import com.google.common.collect.Lists;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@ActiveProfiles(value = TestConstants.TEST_PROFILE)
public class MapServiceImplTest {

	@Autowired
	private MapServiceImpl mapService;

	@Mock
	private PathService pathService;

	@InjectMocks
	private MapServiceImpl mapServiceWithMocks;

	@Test
	public void testCreateMap() {
		final ArrayList<DefaultPath> paths = Lists.newArrayList(//
				new DefaultPath("A", "B", 10), //
				new DefaultPath("B", "C", 15), //
				new DefaultPath("C", "D", 20), //
				new DefaultPath("D", "E", 30));
		mapService.createMap(TestDataFactory.newMapName(), paths);
	}

	@Test
	public void testFindShippingPlan() throws UnknownCityException, UnknownRouteException {
		initMocks(this);
		final ArrayList<DefaultPath> paths = Lists.newArrayList(//
				new DefaultPath("A", "B", 10), //
				new DefaultPath("B", "C", 15));
		final Route route = new DefaultRoute(paths);
		when(pathService.findBestRoute("teste", "A", "C")).thenReturn(route);
		final ShippingPlan shippingPlan = mapServiceWithMocks.findShippingPlan("teste", "A", "C", BigDecimal.TEN,
				new BigDecimal(2.5));
		TestCase.assertEquals(shippingPlan.getShippingCost(), new BigDecimal(6.25).setScale(2));
	}
}
